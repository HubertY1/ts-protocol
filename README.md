# tsprotocol

declaratively specify a protocol then implement it with message types inferred

You must bind the protocol to a `Communicator` object which is a basic abstraction over the underlying channel. The `Communicator` must implement `send(msg: string)` which sends a message on the channel, and `receive((msg: string) => void)` which attaches an event handler that gets passed messages arriving on the channel.

Example:

```typescript
//shared.ts
import { ProtocolBuilder } from "tsprotocol";
export const myProtocol = ProtocolBuilder
            .begin("Alice sends a number", 100)
            .reply("Bob converts it to a string", "100")
            .followup("Alice sends the length of the string", { len: 3 })
            .reply("Bob sends OK if the length is correct", { ok: true })
            .build("myProtocol");

//Alice.ts
import { myProtocol } from "./shared"
const P = myProtocol.bind(AliceCommunicator);
const phase1 = await P.call(1337);
//phase1.payload is type string
const phase2 = await phase1.reply({ len: phase1.payload.length });
//phase2.payload is type { ok: boolean }
console.log(phase2.payload.ok);

//Bob.ts
import { myProtocol } from "./shared"
const P = myProtocol.bind(BobCommunicator);
const phase1 = await P.receive();
//phase1.payload is type number
const stringed = phase1.payload.toString()
const phase2 = await phase1.reply(stringed);
//phase2.payload is type { len: number }
await phase2.reply({ ok: phase2.payload == stringed.length });
```