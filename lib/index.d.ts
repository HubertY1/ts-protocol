declare type Rest<A extends unknown[]> = A extends [unknown, ...infer A2] ? A2 : never;
export declare const ProtocolBuilder: {
    begin<T = void>(desc: string, obj?: T | undefined): ProtocolBuilderReply<[T], []>;
};
interface ProtocolBuilderReply<A extends unknown[], B extends unknown[]> {
    reply<T = void>(desc: string, obj?: T): ProtocolBuilderFollowup<A, [...B, T]>;
    build(id: string): Protocol<A, B>;
}
interface ProtocolBuilderFollowup<A extends unknown[], B extends unknown[]> {
    followup<T = void>(desc: string, obj?: T): ProtocolBuilderReply<[...A, T], B>;
    build(id: string): Protocol<A, B>;
}
export declare class Communicator {
    private _send;
    handlers: {
        [key: string]: Set<(payload: string) => void>;
    };
    emit(type: string, payload?: string): void;
    on(type: string, cb: (payload: string) => void): () => void;
    constructor(_send: (msg: string) => void, receive: (fn: (msg: string) => void) => void);
}
export declare class Protocol<A extends unknown[], B extends unknown[]> {
    private Adefaults;
    private Bdefaults;
    private communicator;
    clearListener: () => void;
    name: string;
    call(payload: A[0]): Promise<[any] extends A ? (B[0] extends (undefined | void) ? ProtocolExecutionNoReplyNoPayload : ProtocolExecutionNoReply<B[0]>) : (B[0] extends (undefined | void) ? ProtocolExecutionNoPayload<Rest<A>, Rest<B>> : ProtocolExecution<Rest<A>, Rest<B>, B[0]>)>;
    receive(cancel: number | PromiseLike<unknown>): Promise<[] extends B ? (A[0] extends (undefined | void) ? ProtocolExecutionNoReplyNoPayload : ProtocolExecutionNoReply<A[0]>) : (A[0] extends (undefined | void) ? ProtocolExecutionNoPayload<B, Rest<A>> : ProtocolExecution<B, Rest<A>, A[0]>)>;
    bind(c: Communicator): Protocol<A, B>;
    constructor(Adefaults: A, Bdefaults: B, name: string, c?: Communicator);
}
interface ProtocolExecutionNoReplyNoPayload {
}
interface ProtocolExecutionNoReply<P> {
    payload: P;
}
interface ProtocolExecutionNoPayload<A extends unknown[], B extends unknown[]> {
    reject(msg: string): void;
    reply(payload: A[0]): Promise<[any] extends A ? (B[0] extends (undefined | void) ? ProtocolExecutionNoReplyNoPayload : ProtocolExecutionNoReply<B[0]>) : B[0] extends (undefined | void) ? ProtocolExecutionNoPayload<Rest<A>, Rest<B>> : ProtocolExecution<Rest<A>, Rest<B>, B[0]>>;
}
declare class ProtocolExecution<A extends unknown[], B extends unknown[], P = void> {
    private Adefaults;
    private Bdefaults;
    private communicator;
    payload: P;
    private commID;
    private alive;
    reject(msg: string): void;
    reply(payload: A[0]): Promise<[any] extends A ? (B[0] extends (undefined | void) ? ProtocolExecutionNoReplyNoPayload : ProtocolExecutionNoReply<B[0]>) : B[0] extends (undefined | void) ? ProtocolExecutionNoPayload<Rest<A>, Rest<B>> : ProtocolExecution<Rest<A>, Rest<B>, B[0]>>;
    constructor(Adefaults: A, Bdefaults: B, commID: string, c: Communicator, payload?: P);
}
export {};
