"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Protocol = exports.Communicator = exports.ProtocolBuilder = void 0;
function immutableCopy(list) {
    return __spreadArray([], list, true);
}
function immutablePopLeft(list) {
    return __spreadArray([], list, true).slice(1);
}
var _ProtocolBuilder = /** @class */ (function () {
    function _ProtocolBuilder(Adefaults, Bdefaults) {
        this.Adefaults = Adefaults;
        this.Bdefaults = Bdefaults;
    }
    _ProtocolBuilder.prototype.reply = function (desc, obj) {
        return new _ProtocolBuilder(immutableCopy(this.Adefaults), __spreadArray(__spreadArray([], this.Bdefaults, true), [
            obj
        ], false));
    };
    _ProtocolBuilder.prototype.followup = function (desc, obj) {
        return new _ProtocolBuilder(__spreadArray(__spreadArray([], this.Adefaults, true), [obj], false), immutableCopy(this.Bdefaults));
    };
    _ProtocolBuilder.prototype.build = function (id) {
        if (id.indexOf(" ") !== -1) {
            throw new TypeError("PANIC: id must not contain space");
        }
        return new Protocol(this.Adefaults, this.Bdefaults, id);
    };
    return _ProtocolBuilder;
}());
exports.ProtocolBuilder = {
    begin: function (desc, obj) {
        return new _ProtocolBuilder([obj], []);
    }
};
var Communicator = /** @class */ (function () {
    function Communicator(_send, receive) {
        var _this = this;
        this.handlers = {};
        this._send = _send;
        receive(function (msg) {
            var sep = msg.indexOf(" ");
            if (sep === -1 || sep === 0) {
                return;
            }
            else {
                var type = msg.slice(0, sep);
                if (!_this.handlers[type] || !_this.handlers[type].size) {
                    return;
                }
                var data_1 = sep === msg.length - 1 ? "" : msg.slice(sep + 1);
                _this.handlers[type].forEach(function (fn) { return fn(data_1); });
            }
        });
    }
    Communicator.prototype.emit = function (type, payload) {
        if (payload === void 0) { payload = ""; }
        this._send("".concat(type, " ").concat(payload));
    };
    Communicator.prototype.on = function (type, cb) {
        var _this = this;
        if (this.handlers[type] === undefined) {
            this.handlers[type] = new Set();
        }
        this.handlers[type].add(cb);
        return function () {
            _this.handlers[type].delete(cb);
        };
    };
    return Communicator;
}());
exports.Communicator = Communicator;
function sendOnCommunicator(c, type, payload) {
    var s = JSON.stringify(payload);
    c.emit(type, s);
}
function delay(ms, msg) {
    if (msg === void 0) { msg = "timeout"; }
    return new Promise(function (resolve) { return setTimeout(function () { resolve(msg); }, ms); });
}
function coerce(expected, received) {
    if (typeof received === "object" && typeof expected === "object") {
        if (Array.isArray(received) && Array.isArray(expected)) {
            var ret = [];
            for (var _i = 0, received_1 = received; _i < received_1.length; _i++) {
                var item = received_1[_i];
                var val = coerce(expected[0], item);
                if (val.error) {
                    return val;
                }
                ret.push(val.result);
            }
            return { result: ret };
        }
        else if (expected !== null && received !== null) {
            var keys = Object.keys(expected);
            var ret = {};
            for (var _a = 0, keys_1 = keys; _a < keys_1.length; _a++) {
                var key = keys_1[_a];
                var val = coerce(expected[key], (received)[key]);
                if (val.error) {
                    return val;
                }
                ret[key] = val.result;
            }
            return { result: ret };
        }
        else if (!(expected === null && received === null)) {
            return { error: TypeError("Coerce failure: null mismatch") };
        }
    }
    if (typeof received === typeof expected) {
        return { result: received };
    }
    else {
        return { error: TypeError("Coerce failure: type ".concat(typeof received, " does not match expected type ").concat(typeof expected)) };
    }
}
function receiveOnCommunicator(c, type, cancel) {
    return __awaiter(this, void 0, void 0, function () {
        var unhook;
        return __generator(this, function (_a) {
            unhook = cancel ? cancel : delay(500);
            return [2 /*return*/, new Promise(function (resolve) {
                    var dismount = c.on(type, function (payload) {
                        dismount();
                        try {
                            var result = JSON.parse(payload);
                            if (typeof result === "object" && result !== null) {
                                resolve({ result: result });
                            }
                            else {
                                resolve({ error: new SyntaxError("JSON.parse did not return an object") });
                            }
                        }
                        catch (_a) {
                            resolve({ error: new SyntaxError("JSON.parse threw on provided message") });
                        }
                    });
                    unhook.then(function (msg) {
                        dismount();
                        resolve({ error: new RangeError(msg) });
                    });
                })];
        });
    });
}
function generateCommID() {
    return Math.floor(Math.random() * 4294967296).toString();
}
function processMessage(expected, data) {
    if (data.err && typeof data.err === "string") {
        return { error: new TypeError("received rejection: " + data.err) };
    }
    else if (data.payload !== undefined) {
        return coerce(expected, data.payload);
    }
    else {
        if (expected === undefined) {
            return { result: expected };
        }
        return { error: new TypeError("received message without expected payload [2]") };
    }
}
var Protocol = /** @class */ (function () {
    function Protocol(Adefaults, Bdefaults, name, c) {
        this.Adefaults = Adefaults;
        this.Bdefaults = Bdefaults;
        this.name = name;
        this.communicator = c;
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        this.clearListener = function () { };
    }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    Protocol.prototype.call = function (payload) {
        return __awaiter(this, void 0, void 0, function () {
            var commID, rv, recv, sanitize;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.communicator === undefined) {
                            throw new Error("PANIC: Communicator is unbound");
                        }
                        commID = generateCommID();
                        rv = receiveOnCommunicator(this.communicator, commID);
                        sendOnCommunicator(this.communicator, this.name, { commID: commID, payload: payload });
                        return [4 /*yield*/, rv];
                    case 1:
                        recv = _a.sent();
                        if (recv.error) {
                            throw recv.error;
                        }
                        sanitize = processMessage(this.Bdefaults[0], recv.result);
                        if (sanitize.error) {
                            throw sanitize.error;
                        }
                        return [2 /*return*/, new ProtocolExecution(immutablePopLeft(this.Adefaults), immutablePopLeft(this.Bdefaults), commID, this.communicator, sanitize.result)];
                }
            });
        });
    };
    Protocol.prototype.receive = function (cancel) {
        return __awaiter(this, void 0, void 0, function () {
            var cancelProm, recv, data, commID, sanitize;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.communicator === undefined) {
                            throw new Error("PANIC: Communicator is unbound");
                        }
                        cancelProm = typeof cancel == "number" ? delay(cancel) : cancel;
                        return [4 /*yield*/, receiveOnCommunicator(this.communicator, this.name, cancelProm.then(function () { return "receive aborted"; }))];
                    case 1:
                        recv = _a.sent();
                        if (recv.error) {
                            throw recv.error;
                        }
                        data = recv.result;
                        if (data.commID === undefined ||
                            typeof data.commID !== "string" ||
                            data.commID.length > 10) {
                            throw new TypeError("reject: received invalid commID [6]");
                        }
                        commID = data.commID;
                        sanitize = processMessage(this.Adefaults[0], data);
                        if (sanitize.error) {
                            throw sanitize.error;
                        }
                        return [2 /*return*/, new ProtocolExecution(immutableCopy(this.Bdefaults), immutablePopLeft(this.Adefaults), commID, this.communicator, sanitize.result)];
                }
            });
        });
    };
    Protocol.prototype.bind = function (c) {
        return new Protocol(this.Adefaults, this.Bdefaults, this.name, c);
    };
    return Protocol;
}());
exports.Protocol = Protocol;
var ProtocolExecution = /** @class */ (function () {
    function ProtocolExecution(Adefaults, Bdefaults, commID, c, payload) {
        this.Adefaults = Adefaults;
        this.Bdefaults = Bdefaults;
        this.commID = commID;
        this.communicator = c;
        this.payload = payload;
        this.alive = true;
    }
    ProtocolExecution.prototype.reject = function (msg) {
        if (this.alive) {
            this.alive = false;
            sendOnCommunicator(this.communicator, this.commID, { err: msg });
        }
        else {
            throw new RangeError("PANIC: execution thread is closed");
        }
    };
    ProtocolExecution.prototype.reply = function (payload
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ) {
        return __awaiter(this, void 0, void 0, function () {
            var rv, recv, sanitize;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.alive) return [3 /*break*/, 2];
                        this.alive = false;
                        if (this.Bdefaults.length === 0) {
                            //no reply expected
                            sendOnCommunicator(this.communicator, this.commID, { payload: payload });
                            return [2 /*return*/, new ProtocolExecution(immutablePopLeft(this.Adefaults), immutablePopLeft(this.Bdefaults), this.commID, this.communicator)];
                        }
                        rv = receiveOnCommunicator(this.communicator, this.commID);
                        sendOnCommunicator(this.communicator, this.commID, { payload: payload });
                        return [4 /*yield*/, rv];
                    case 1:
                        recv = _a.sent();
                        if (recv.error) {
                            throw recv.error;
                        }
                        sanitize = processMessage(this.Bdefaults[0], recv.result);
                        if (sanitize.error) {
                            throw sanitize.error;
                        }
                        return [2 /*return*/, new ProtocolExecution(immutablePopLeft(this.Adefaults), immutablePopLeft(this.Bdefaults), this.commID, this.communicator, sanitize.result)];
                    case 2: throw new RangeError("PANIC: execution thread is closed");
                }
            });
        });
    };
    return ProtocolExecution;
}());
