import { Communicator, ProtocolBuilder } from "../index";

class DummyComm {
    handlers: { [key: string]: ((payload: string) => void)[] } = {};
    remote?: DummyComm = undefined;
    _receive?: (payload: string) => void = undefined;
    send(payload: string): void {
        if (this.remote && this.remote._receive) {
            this.remote._receive(payload);
        }
    }
    receive(cb: (payload: string) => void) {
        this._receive = cb;
    }
    connect(remote: DummyComm) {
        this.remote = remote;
    }
    toCommunicator() {
        return new Communicator(this.send.bind(this), this.receive.bind(this));
    }
}

test("Protocol test", () => {
    return (async () => {
        const p1 = new DummyComm();
        const p2 = new DummyComm();
        p1.connect(p2);
        p2.connect(p1);

        const P = ProtocolBuilder
            .begin("P1 sends a number", 1)
            .reply("P2 sends the stringed version", "1")
            .followup("P1 sends the length of the string", { len: 1 })
            .reply("P2 sends OK", { ok: true })
            .build("P");

        const proc1 = (async () => {
            const phase1 = await P.bind(p2.toCommunicator()).receive(1000);
            expect(phase1.payload).toStrictEqual(100);
            const phase2 = await phase1.reply(phase1.payload.toString());
            expect(phase2.payload).toStrictEqual({ len: 3 });
            await phase2.reply({ ok: true });
        })();

        const proc2 = (async () => {
            const phase1 = await P.bind(p1.toCommunicator()).call(100);
            expect(phase1.payload).toStrictEqual("100");
            const phase2 = await phase1.reply({ len: 3 });
            expect(phase2.payload).toStrictEqual({ ok: true });
        })();
        await proc1;
        await proc2;
    })();
});

test("empty string test", () => {
    return (async () => {
        const p1 = new DummyComm();
        const p2 = new DummyComm();
        p1.connect(p2);
        p2.connect(p1);

        const P = ProtocolBuilder
            .begin("begin", "a")
            .reply("ack", "a")
            .followup("ack", "a")
            .build("noargs-protocol");

        const proc1 = (async () => {
            const phase1 = await P.bind(p2.toCommunicator()).receive(1000);
            expect(phase1.payload).toStrictEqual("");
            const phase2 = await phase1.reply("");
            expect(phase2.payload).toStrictEqual("");
        })();

        const proc2 = (async () => {
            const phase1 = await P.bind(p1.toCommunicator()).call("");
            expect(phase1.payload).toStrictEqual("");
            await phase1.reply("");
        })();
        await proc1;
        await proc2;
    })();
});

test("array test", () => {
    return (async () => {
        const p1 = new DummyComm();
        const p2 = new DummyComm();
        p1.connect(p2);
        p2.connect(p1);

        const P = ProtocolBuilder
            .begin("begin", [0, 0, 0])
            .reply("ack", ["0", "0", "0"])
            .build("array-protocol");

        const proc1 = (async () => {
            const phase1 = await P.bind(p2.toCommunicator()).receive(1000);
            expect(phase1.payload).toStrictEqual([1, 2, 3]);
            phase1.reply(["1", "2", "3"]);
        })();

        const proc2 = (async () => {
            const phase1 = await P.bind(p1.toCommunicator()).call([1, 2, 3]);
            expect(phase1.payload).toStrictEqual(["1", "2", "3"]);
        })();
        await proc1;
        await proc2;
    })();
});