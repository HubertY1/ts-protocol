type ErrWrapped<T> = { error: Error } | { error?: undefined, result: T }
type Rest<A extends unknown[]> = A extends [unknown, ...infer A2] ? A2 : never;

function immutableCopy<A extends unknown[]>(list: A): A {
    return [...list] as A;
}

function immutablePopLeft<A extends unknown[]>(list: A): Rest<A> {
    return [...list].slice(1) as Rest<A>;
}

class _ProtocolBuilder<A extends unknown[], B extends unknown[]> {
    private Adefaults: A;
    private Bdefaults: B;
    reply<T = void>(desc: string, obj?: T): ProtocolBuilderFollowup<A, [...B, T]> {
        return new _ProtocolBuilder(immutableCopy(this.Adefaults), [
            ...this.Bdefaults,
            obj as T
        ]);
    }
    followup<T = void>(desc: string, obj?: T): ProtocolBuilderReply<[...A, T], B> {
        return new _ProtocolBuilder(
            [...this.Adefaults, obj as T],
            immutableCopy(this.Bdefaults)
        );
    }
    build(id: string): Protocol<A, B> {
        if (id.indexOf(" ") !== -1) {
            throw new TypeError("PANIC: id must not contain space");
        }
        return new Protocol(this.Adefaults, this.Bdefaults, id);
    }
    constructor(Adefaults: A, Bdefaults: B) {
        this.Adefaults = Adefaults;
        this.Bdefaults = Bdefaults;
    }
}

export const ProtocolBuilder = {
    begin<T = void>(desc: string, obj?: T): ProtocolBuilderReply<[T], []> {
        return new _ProtocolBuilder([obj as T], []);
    }
};

interface ProtocolBuilderReply<A extends unknown[], B extends unknown[]> {
    reply<T = void>(desc: string, obj?: T): ProtocolBuilderFollowup<A, [...B, T]>;
    build(id: string): Protocol<A, B>;
}
interface ProtocolBuilderFollowup<A extends unknown[], B extends unknown[]> {
    followup<T = void>(desc: string, obj?: T): ProtocolBuilderReply<[...A, T], B>;
    build(id: string): Protocol<A, B>;
}

export class Communicator {
    private _send: (msg: string) => void;
    handlers: { [key: string]: Set<(payload: string) => void> } = {};
    emit(type: string, payload = ""): void {
        this._send(`${type} ${payload}`);
    }
    on(type: string, cb: (payload: string) => void): () => void {
        if (this.handlers[type] === undefined) {
            this.handlers[type] = new Set();
        }
        this.handlers[type].add(cb);
        return () => {
            this.handlers[type].delete(cb);
        }
    }
    constructor(_send: (msg: string) => void, receive: (fn: (msg: string) => void) => void) {
        this._send = _send;
        receive((msg) => {
            const sep = msg.indexOf(" ");
            if (sep === -1 || sep === 0) {
                return;
            }
            else {
                const type = msg.slice(0, sep);
                if (!this.handlers[type] || !this.handlers[type].size) {
                    return;
                }
                const data = sep === msg.length - 1 ? "" : msg.slice(sep + 1);
                this.handlers[type].forEach(fn => fn(data));
            }
        });
    }
}

function sendOnCommunicator(c: Communicator, type: string, payload: object) {
    const s = JSON.stringify(payload);
    c.emit(type, s);
}

function delay(ms: number, msg = "timeout"): Promise<string> {
    return new Promise((resolve) => setTimeout(() => { resolve(msg) }, ms));
}

function coerce<T>(expected: T, received: unknown): ErrWrapped<T> {
    if (typeof received === "object" && typeof expected === "object") {
        if (Array.isArray(received) && Array.isArray(expected)) {
            const ret = [];
            for (const item of received) {
                const val = coerce(expected[0], item);
                if (val.error) {
                    return val
                }
                ret.push(val.result);
            }
            return { result: <unknown>ret as T };
        }
        else if (expected !== null && received !== null) {
            const keys = Object.keys(<unknown>expected as object);
            const ret = {} as { [key: string]: unknown };
            for (const key of keys) {
                const val = coerce(
                    (<{ [key: string]: unknown }>(<unknown>expected))[key],
                    (<{ [key: string]: unknown }>(received))[key]
                );
                if (val.error) {
                    return val
                }
                ret[key] = val.result;
            }
            return { result: <unknown>ret as T };
        }
        else if (!(expected === null && received === null)) {
            return { error: TypeError(`Coerce failure: null mismatch`) };
        }
    }
    if (typeof received === typeof expected) {
        return { result: received as T };
    } else {
        return { error: TypeError(`Coerce failure: type ${typeof received} does not match expected type ${typeof expected}`) }
    }
}

async function receiveOnCommunicator(
    c: Communicator,
    type: string,
    cancel?: PromiseLike<string>
): Promise<ErrWrapped<object>> {
    const unhook = cancel ? cancel : delay(500);
    return new Promise((resolve) => {
        const dismount = c.on(type, (payload: string) => {
            dismount();
            try {
                const result = JSON.parse(payload);
                if (typeof result === "object" && result !== null) {
                    resolve({ result });
                }
                else {
                    resolve({ error: new SyntaxError("JSON.parse did not return an object") });
                }
            }
            catch {
                resolve({ error: new SyntaxError("JSON.parse threw on provided message") });
            }

        });
        unhook.then((msg) => {
            dismount();
            resolve({ error: new RangeError(msg) })
        });
    });
}

function generateCommID(): string {
    return Math.floor(Math.random() * 4294967296).toString();
}
function processMessage<T>(expected: T, data: { err?: unknown, payload?: unknown }): ErrWrapped<T> {
    if (data.err && typeof data.err === "string") {
        return { error: new TypeError("received rejection: " + data.err) };
    } else if (data.payload !== undefined) {
        return coerce(expected, data.payload);
    } else {
        if (expected === undefined) {
            return { result: expected };
        }
        return { error: new TypeError("received message without expected payload [2]") };
    }
}

export class Protocol<A extends unknown[], B extends unknown[]> {
    private Adefaults: A;
    private Bdefaults: B;
    private communicator: Communicator | undefined;
    clearListener: () => void
    name: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    async call(payload: A[0]): Promise<[any] extends A ? (B[0] extends (undefined | void) ? ProtocolExecutionNoReplyNoPayload : ProtocolExecutionNoReply<B[0]>)
        : (B[0] extends (undefined | void) ? ProtocolExecutionNoPayload<Rest<A>, Rest<B>> : ProtocolExecution<Rest<A>, Rest<B>, B[0]>)> {
        if (this.communicator === undefined) {
            throw new Error("PANIC: Communicator is unbound");
        }
        const commID = generateCommID();

        const rv = receiveOnCommunicator(this.communicator, commID);

        sendOnCommunicator(this.communicator, this.name, { commID, payload });
        const recv = await rv;
        if (recv.error) {
            throw recv.error;
        }
        const sanitize = processMessage(
            this.Bdefaults[0],
            recv.result
        );
        if (sanitize.error) {
            throw sanitize.error;
        }
        return new ProtocolExecution(
            immutablePopLeft(this.Adefaults),
            immutablePopLeft(this.Bdefaults),
            commID,
            this.communicator,
            sanitize.result
        );
    }
    async receive(cancel: number | PromiseLike<unknown>): Promise<[] extends B ? (A[0] extends (undefined | void) ? ProtocolExecutionNoReplyNoPayload : ProtocolExecutionNoReply<A[0]>)
        : (A[0] extends (undefined | void) ? ProtocolExecutionNoPayload<B, Rest<A>> : ProtocolExecution<B, Rest<A>, A[0]>)> {
        if (this.communicator === undefined) {
            throw new Error("PANIC: Communicator is unbound");
        }
        const cancelProm = typeof cancel == "number" ? delay(cancel) : cancel;
        const recv = await receiveOnCommunicator(this.communicator, this.name, cancelProm.then(() => { return "receive aborted" }));
        if (recv.error) {
            throw recv.error;
        }
        const data = recv.result as { commID?: unknown };
        if (
            data.commID === undefined ||
            typeof data.commID !== "string" ||
            data.commID.length > 10
        ) {
            throw new TypeError("reject: received invalid commID [6]");
        }
        const commID = data.commID as string;
        const sanitize = processMessage(this.Adefaults[0], data as object);
        if (sanitize.error) {
            throw sanitize.error;
        }
        return new ProtocolExecution(
            immutableCopy(this.Bdefaults),
            immutablePopLeft(this.Adefaults),
            commID,
            this.communicator,
            sanitize.result
        );
    }
    bind(c: Communicator) {
        return new Protocol(this.Adefaults, this.Bdefaults, this.name, c)
    }
    constructor(Adefaults: A, Bdefaults: B, name: string, c?: Communicator) {
        this.Adefaults = Adefaults;
        this.Bdefaults = Bdefaults;
        this.name = name;
        this.communicator = c;
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        this.clearListener = () => { };
    }
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ProtocolExecutionNoReplyNoPayload {

}

interface ProtocolExecutionNoReply<P> {
    payload: P;
}

interface ProtocolExecutionNoPayload<A extends unknown[], B extends unknown[]> {
    reject(msg: string): void
    reply(
        payload: A[0]
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ): Promise<[any] extends A ? (B[0] extends (undefined | void) ? ProtocolExecutionNoReplyNoPayload : ProtocolExecutionNoReply<B[0]>)
        : B[0] extends (undefined | void) ? ProtocolExecutionNoPayload<Rest<A>, Rest<B>> : ProtocolExecution<Rest<A>, Rest<B>, B[0]>>
}


class ProtocolExecution<A extends unknown[], B extends unknown[], P = void> {
    private Adefaults: A;
    private Bdefaults: B;
    private communicator: Communicator;
    payload: P;
    private commID: string;
    private alive: boolean;
    reject(msg: string): void {
        if (this.alive) {
            this.alive = false;
            sendOnCommunicator(this.communicator, this.commID, { err: msg });
        } else {
            throw new RangeError("PANIC: execution thread is closed");
        }
    }
    async reply(
        payload: A[0]
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ): Promise<[any] extends A ? (B[0] extends (undefined | void) ? ProtocolExecutionNoReplyNoPayload : ProtocolExecutionNoReply<B[0]>)
        : B[0] extends (undefined | void) ? ProtocolExecutionNoPayload<Rest<A>, Rest<B>> : ProtocolExecution<Rest<A>, Rest<B>, B[0]>> {
        if (this.alive) {
            this.alive = false;

            if (this.Bdefaults.length === 0) {
                //no reply expected
                sendOnCommunicator(this.communicator, this.commID, { payload });
                return new ProtocolExecution(
                    immutablePopLeft(this.Adefaults),
                    immutablePopLeft(this.Bdefaults),
                    this.commID,
                    this.communicator
                );
            }

            const rv = receiveOnCommunicator(this.communicator, this.commID);
            sendOnCommunicator(this.communicator, this.commID, { payload });
            const recv = await rv;
            if (recv.error) {
                throw recv.error;
            }
            const sanitize = processMessage(this.Bdefaults[0], recv.result);
            if (sanitize.error) {
                throw sanitize.error;
            }
            return new ProtocolExecution(
                immutablePopLeft(this.Adefaults),
                immutablePopLeft(this.Bdefaults),
                this.commID,
                this.communicator,
                sanitize.result
            );
        } else {
            throw new RangeError("PANIC: execution thread is closed");
        }
    }
    constructor(
        Adefaults: A,
        Bdefaults: B,
        commID: string,
        c: Communicator,
        payload?: P
    ) {
        this.Adefaults = Adefaults;
        this.Bdefaults = Bdefaults;
        this.commID = commID;
        this.communicator = c;
        this.payload = payload as P;
        this.alive = true;
    }
}
